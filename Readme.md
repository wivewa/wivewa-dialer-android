# wivewa-dialer-android

Welefon is a Phone App that is simple and modern at the same time. You can dial numbers and receive calls. It does not do anything you do not need, e.g. provide a contact list that your system contact App alrady contains.

The special feature of Welefon is an API that allows a seamlessly integration of other apps in your phone experience. The Welefon source code contains an example App that uses the API in the folder apidemoapp.

Using this API, other Apps can receive details about the current calls. Like the deprecated PhoneStateListener of Android, this contains the phone number. In contrast to the PhoneStateListener, you do not need to fear the removal of the API. Additionally, Welefon includes details of calls in the background, e.g. calls that are in the state "hold".

Welefon can receive additional caller details from other Apps. This can be a caller name, a caller details action and a RemoteView. The received caller name is shown as you expect it, completly independent of the system contact list. If a detail action is provided, then you can tap on the name to jump directly to the right place in the other App.

The RemoteViews are the method that allows Widgets on your homescreen. The same method can be used to show additional details and controls in the call screen. This allows showing any extra details.

This APIs provide many possibilites. One is the integration with the phone system of wivewa.de. This allows viewing and annotating current and past phone calls in the Browser. Moreover, you can see the caller name and the last ticket assigned to this caller at the incoming call screen.

Of course this is safe and secure: The Welefon API can only be used with your permission.

## Screenshots

take a look at ``app/src/main/play/en-US/listing/phoneScreenshots``

## Download

<https://f-droid.org/en/packages/de.wivewa.dialer/>

## License

```
wivewa-dialer-android - a dialer application for Android with APIs
Copyright (C) 2023 - 2024 Jonas Lochmann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.
```
