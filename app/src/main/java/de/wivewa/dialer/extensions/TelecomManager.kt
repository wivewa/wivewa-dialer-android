/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.extensions

import android.net.Uri
import android.os.Bundle
import android.telecom.TelecomManager
import de.wivewa.dialer.platform.CallExtras
import de.wivewa.dialer.service.CallService
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.withTimeoutOrNull
import java.util.*

@Throws(SecurityException::class)
suspend fun TelecomManager.placeCallAndWait(uri: Uri, extras: Bundle?, outgoingCallExtras: Bundle?): Boolean {
    val callId = UUID.randomUUID().toString()

    this.placeCall(
        uri,
        Bundle(extras ?: Bundle()).also { bundle ->
            bundle.putBundle(TelecomManager.EXTRA_OUTGOING_CALL_EXTRAS, Bundle(outgoingCallExtras ?: Bundle()).also {
                it.putString(CallExtras.CALL_ID, callId)
            })
        }
    )

    return withTimeoutOrNull(5 * 1000) {
        CallService.currentCalls.firstOrNull { calls ->
            calls.any { it.id == callId }
        }

        true
    } ?: false
}