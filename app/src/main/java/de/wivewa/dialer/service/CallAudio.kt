/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.service

import android.bluetooth.BluetoothDevice

data class CallAudio (
    val isMuted: Boolean,
    val toggleMute: () -> Unit,
    val currentRoutes: List<Route>,
    val availableRoutes: List<Route>,
    val useAudioRoute: (Route) -> Unit
) {
    sealed class Route {
        data class Bluetooth(val device: BluetoothDevice): Route()
        object Earpiece: Route()
        object Speaker: Route()
        object WiredHeadset: Route()
        object WiredHeadsetOrEarpiece: Route()
    }
}