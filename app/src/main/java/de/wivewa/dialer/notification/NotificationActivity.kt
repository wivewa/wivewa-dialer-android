/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.notification

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import de.wivewa.dialer.service.CallInfo
import de.wivewa.dialer.service.CallService
import de.wivewa.dialer.ui.call.CallActivity
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch

class NotificationActivity: Activity() {
    companion object {
        const val ACTION_ACCEPT_CALL = "accept"
    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            when (intent.action) {
                ACTION_ACCEPT_CALL -> {
                    GlobalScope.launch {
                        CallService.currentCalls.firstOrNull()
                            ?.let { calls -> CallInfo.getRinging(calls) }
                            ?.let { ringing -> ringing.actions.answer() }
                    }

                    startActivity(
                        Intent(this, CallActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    )
                }
            }
        }

        finish()
    }
}