/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import de.wivewa.dialer.ui.call.CallActivity

object PendingIntents {
    private const val FLAGS =
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT

    private const val REJECT_RINGING_CALL = 1
    private const val ANSWER_RINGING_CALL = 2
    private const val MUTE_RINGING_CALL = 3
    private const val DISCONNECT_CURRENT_CALL = 4
    private const val SHOW_IN_CALL_UI = 5

    private fun notificationReceiverIntent(context: Context, id: Int, action: String): PendingIntent = PendingIntent.getBroadcast(
        context,
        id,
        Intent(context, NotificationReceiver::class.java).setAction(action),
        FLAGS
    )

    fun rejectRingingCall(context: Context) = notificationReceiverIntent(
        context,
        REJECT_RINGING_CALL,
        NotificationReceiver.ACTION_REJECT_RINGING_CALL
    )

    fun answerRingingCall(context: Context): PendingIntent = PendingIntent.getActivity(
        context,
        ANSWER_RINGING_CALL,
        Intent(context, NotificationActivity::class.java).setAction(NotificationActivity.ACTION_ACCEPT_CALL),
        FLAGS
    )

    fun muteRingingCall(context: Context) = notificationReceiverIntent(
        context,
        MUTE_RINGING_CALL,
        NotificationReceiver.ACTION_MUTE_RINGING_CALL
    )

    fun disconnectCurrentCall(context: Context): PendingIntent = notificationReceiverIntent(
        context,
        DISCONNECT_CURRENT_CALL,
        NotificationReceiver.ACTION_DISCONNECT_CURRENT_CALL
    )

    fun showInCallUi(context: Context): PendingIntent = PendingIntent.getActivity(
        context,
        SHOW_IN_CALL_UI,
        Intent(context, CallActivity::class.java),
        FLAGS
    )
}