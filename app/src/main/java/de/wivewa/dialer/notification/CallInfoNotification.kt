/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.notification

import android.content.Context
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallInfo

object CallInfoNotification {
    fun getDisplayName(context: Context, callInfo: CallInfo): String =
        if (callInfo.hasChildren) context.getString(R.string.call_conference)
        else callInfo.integrationDisplayName
            ?: callInfo.remoteDisplayName
            ?: callInfo.remotePhoneNumber
            ?: context.getString(R.string.call_unknown_caller)
}