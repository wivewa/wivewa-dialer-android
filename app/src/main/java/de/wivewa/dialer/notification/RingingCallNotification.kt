/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.notification

import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.Person
import androidx.core.content.getSystemService
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallInfo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

object RingingCallNotification {
    private const val ID = NotificationIds.RINGING_CALL

    suspend fun process(
        context: Context,
        calls: Flow<List<CallInfo>>
    ) {
        val notificationManager = context.getSystemService<NotificationManager>()!!

        try {
            calls
                .map { CallInfo.getRinging(it) }
                .distinctUntilChanged()
                .collect { ringingCall ->
                    if (ringingCall == null) notificationManager.cancel(ID)
                    else {
                        val channel =
                            if (ringingCall.silenceRinging) NotificationChannels.RINGING_CALL_LOW
                            else NotificationChannels.RINGING_CALL

                        val showUi = PendingIntents.showInCallUi(context)
                        val answer = PendingIntents.answerRingingCall(context)
                        val reject = PendingIntents.rejectRingingCall(context)
                        val mute = PendingIntents.muteRingingCall(context)

                        val person = Person.Builder()
                            .setName(CallInfoNotification.getDisplayName(context, ringingCall))
                            .build()

                        val muteAction = NotificationCompat.Action.Builder(
                            R.drawable.ic_stat_volume_off,
                            context.getString(R.string.notification_ringing_action_mute),
                            mute
                        ).setShowsUserInterface(false).build()

                        notificationManager.notify(
                            ID,
                            NotificationCompat.Builder(context, channel)
                                .setSmallIcon(R.drawable.ic_stat_phone)
                                .setCategory(NotificationCompat.CATEGORY_CALL)
                                .setOnlyAlertOnce(true)
                                .setContentIntent(showUi)
                                .setFullScreenIntent(showUi, false)
                                .let { builder ->
                                    if (ringingCall.silenceRinging)
                                        builder
                                            .setOngoing(true)
                                    else
                                        builder
                                            .addAction(muteAction)
                                            .setOngoing(false)
                                            .setDeleteIntent(mute)
                                }
                                .setStyle(NotificationCompat.CallStyle.forIncomingCall(person, reject, answer))
                                .build()
                        )
                    }
                }
        } finally {
            notificationManager.cancel(ID)
        }
    }
}