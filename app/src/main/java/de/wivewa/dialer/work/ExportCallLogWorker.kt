/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.work

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.CallLog
import android.util.JsonWriter
import android.util.Log
import android.widget.Toast
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import de.wivewa.dialer.BuildConfig
import de.wivewa.dialer.R
import java.io.OutputStreamWriter

class ExportCallLogWorker(context: Context, workerParameters: WorkerParameters): Worker(context, workerParameters) {
    companion object {
        private const val LOG_TAG = "ExportCallLogWorker"
        private const val EXPORT_FILE_URI = "export file uri"
        private val handler = Handler(Looper.getMainLooper())

        fun enqueue(workManager: WorkManager, uri: Uri) {
            workManager.enqueue(
                OneTimeWorkRequestBuilder<ExportCallLogWorker>()
                    .setInputData(Data.Builder().putString(EXPORT_FILE_URI, uri.toString()).build())
                    .build()
            )
        }
    }

    override fun doWork(): Result {
        val exportUri = Uri.parse(inputData.getString(EXPORT_FILE_URI))

        handler.post {
            Toast.makeText(applicationContext, R.string.call_log_export_toast_start, Toast.LENGTH_SHORT).show()
        }

        try {
            applicationContext.contentResolver.openOutputStream(exportUri, "wt").use { output ->
                OutputStreamWriter(output).use { streamWriter ->
                    JsonWriter(streamWriter).use { jsonWriter ->
                        jsonWriter.setIndent("  ")
                        jsonWriter.beginObject()

                        jsonWriter.name("generator").beginObject()
                            .name("applicationId").value(BuildConfig.APPLICATION_ID)
                            .name("version").value(BuildConfig.VERSION_NAME)
                            .name("time").value(System.currentTimeMillis())
                            .endObject()

                        jsonWriter.name("calls").beginArray()
                        applicationContext.contentResolver.query(
                            CallLog.Calls.CONTENT_URI,
                            arrayOf(
                                CallLog.Calls._ID,
                                CallLog.Calls.DATE,
                                CallLog.Calls.TYPE,
                                CallLog.Calls.DURATION,
                                CallLog.Calls.NUMBER
                            ),
                            null,
                            null,
                            null
                        )?.use { cursor ->
                            val idColumn = cursor.getColumnIndexOrThrow(CallLog.Calls._ID)
                            val dateColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.DATE)
                            val typeColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.TYPE)
                            val durationColumn =
                                cursor.getColumnIndexOrThrow(CallLog.Calls.DURATION)
                            val numberColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.NUMBER)

                            if (cursor.moveToFirst()) do {
                                val type = when (cursor.getInt(typeColumn)) {
                                    CallLog.Calls.INCOMING_TYPE -> "incoming"
                                    CallLog.Calls.MISSED_TYPE -> "missed"
                                    CallLog.Calls.OUTGOING_TYPE -> "outgoing"
                                    CallLog.Calls.VOICEMAIL_TYPE -> "mailbox"
                                    CallLog.Calls.BLOCKED_TYPE -> "blocked"
                                    CallLog.Calls.ANSWERED_EXTERNALLY_TYPE -> "externally answered"
                                    CallLog.Calls.REJECTED_TYPE -> "rejected"
                                    else -> "unknown"
                                }

                                jsonWriter.beginObject()
                                    .name("id").value(cursor.getLong(idColumn))
                                    .name("time").value(cursor.getLong(dateColumn))
                                    .name("type").value(type)
                                    .name("duration").value(cursor.getLong(durationColumn))
                                    .name("number").value(cursor.getString(numberColumn))
                                    .endObject()
                            } while (cursor.moveToNext())
                        }
                        jsonWriter.endArray()

                        jsonWriter.endObject()
                    }
                }
            }

            handler.post {
                Toast.makeText(applicationContext, R.string.call_log_export_toast_finished, Toast.LENGTH_SHORT).show()
            }

            return Result.success()
        } catch (ex: Exception) {
            if (BuildConfig.DEBUG) {
                Log.w(LOG_TAG, "export failed", ex)
            }

            handler.post {
                Toast.makeText(applicationContext, R.string.call_log_export_toast_failed, Toast.LENGTH_SHORT).show()
            }

            return Result.failure()
        }
    }
}