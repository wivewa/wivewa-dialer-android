/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.data

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager

class AppSettings (private val sharedPreferences: SharedPreferences) {
    companion object {
        private const val AUTO_AUDIO_MODE = "aa_mode"

        private val lock = Object()
        private var instance: AppSettings? = null

        fun with(application: Application): AppSettings {
            if (instance == null) {
                synchronized(lock) {
                    if (instance == null) {
                        instance = AppSettings(PreferenceManager.getDefaultSharedPreferences(application))
                    }
                }
            }

            return instance!!
        }
    }

    var autoAudioMode
        get() = sharedPreferences.getBoolean(AUTO_AUDIO_MODE, false)
        set(value) {
            sharedPreferences.edit()
                .putBoolean(AUTO_AUDIO_MODE, value)
                .apply()
        }
}