/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.data

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.provider.ContactsContract
import android.telephony.PhoneNumberUtils
import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke

object SystemContacts {
    suspend fun query(context: Context, phoneNumber: String): Item? =
        query(context, listOf(phoneNumber))[phoneNumber]

    suspend fun query(context: Context, phoneNumbers: Collection<String>): Map<String, Item> = Dispatchers.IO.invoke {
        val normalizedPhoneNumbers = phoneNumbers.groupBy {
            PhoneNumberUtils.normalizeNumber(it)
        }.filterKeys { it.isNotEmpty() }.toMutableMap()

        if (normalizedPhoneNumbers.isEmpty()) return@invoke emptyMap()
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) return@invoke emptyMap()

        val defaultCountry = context.getSystemService<TelephonyManager>()!!.networkCountryIso

        val results = mutableMapOf<String, Item>()

        context.contentResolver.query(
            ContactsContract.Data.CONTENT_URI,
            arrayOf(ContactsContract.Data.DISPLAY_NAME, ContactsContract.Data.DATA1),
            "${ContactsContract.Data.MIMETYPE} = ?",
            arrayOf(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE),
            null
        ).use { cursor ->
            if (cursor != null && cursor.moveToFirst()) {
                val displayNameColumn = cursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME)
                val dataColumn = cursor.getColumnIndexOrThrow(ContactsContract.Data.DATA1)

                do {
                    val displayName = cursor.getString(displayNameColumn)
                    val data = cursor.getString(dataColumn)

                    val iterator = normalizedPhoneNumbers.iterator()

                    while (iterator.hasNext()) {
                        val queryItem = iterator.next()
                        val normalizedPhoneNumber = queryItem.key

                        val match =
                            if (VERSION.SDK_INT >= VERSION_CODES.S) PhoneNumberUtils.areSamePhoneNumber(normalizedPhoneNumber, data, defaultCountry)
                            else PhoneNumberUtils.compare(normalizedPhoneNumber, data)

                        if (match) {
                            val item = Item(displayName = displayName)

                            queryItem.value.forEach { results[it] = item }

                            iterator.remove()
                        }
                    }
                } while (cursor.moveToNext() && normalizedPhoneNumbers.isNotEmpty())
            }
        }

        results
    }

    data class Item(val displayName: String)
}