/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.content.Context
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.ui.graphics.vector.ImageVector
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallAudio

data class CallAudioPresentation(
    val icon: ImageVector,
    val shortText: String,
    val longText: String = shortText
) {
    companion object {
        fun forRoute(route: CallAudio.Route, context: Context): CallAudioPresentation = when (route) {
            is CallAudio.Route.Bluetooth -> CallAudioPresentation(
                Icons.Default.Bluetooth,
                "Bluetooth",
                try {
                    route.device.name
                } catch (ex: SecurityException) {
                    route.device.address
                }
            )
            CallAudio.Route.Earpiece -> CallAudioPresentation(
                Icons.Default.Phone,
                context.getString(R.string.call_audio_earpiece)
            )
            CallAudio.Route.Speaker -> CallAudioPresentation(
                Icons.Default.VolumeUp,
                context.getString(R.string.call_audio_speaker)
            )
            CallAudio.Route.WiredHeadset -> CallAudioPresentation(
                Icons.Default.Headphones,
                context.getString(R.string.call_aduio_headset)
            )
            CallAudio.Route.WiredHeadsetOrEarpiece -> CallAudioPresentation(
                Icons.Default.Phone,
                context.getString(R.string.call_audio_earpiece),
                context.getString(R.string.call_audio_earpiece_or_headset)
            )
        }
    }
}