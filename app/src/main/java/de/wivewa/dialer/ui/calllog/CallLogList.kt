/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import android.telecom.PhoneAccountHandle
import de.wivewa.dialer.R
import android.text.format.DateUtils
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.itemKey
import de.wivewa.dialer.ui.padding.PaddingValueSum

@Composable
fun CallLogList(
    list: LazyPagingItems<CallLogItem>,
    expandedCall: ExpandedCall?,
    updateExpandedCall: (ExpandedCall?) -> Unit,
    actions: CallLogItemActions,
    now: Long,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues()
) {
    LazyColumn (
        verticalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValueSum(contentPadding, PaddingValues(8.dp)),
        modifier = modifier
    ) {
        items(
            count = list.itemCount,
            key = list.itemKey { it.id }
        ) { index ->
            val item = list[index]

            val expandState =
                if (expandedCall == null || item == null || item.id != expandedCall.id) CallExpandState.None
                else if (expandedCall.more) CallExpandState.Full
                else CallExpandState.Basic

            CallLogListItem(
                item = item,
                now = now,
                expandState = expandState,
                onUpdateExpanded = { expand -> when (expand) {
                    CallExpandState.None -> updateExpandedCall(null)
                    CallExpandState.Basic -> item?.id?.also { updateExpandedCall(ExpandedCall(it, false)) }
                    CallExpandState.Full -> item?.id?.also { updateExpandedCall(ExpandedCall(it, true)) }
                } },
                actions = actions
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LazyItemScope.CallLogListItem(
    item: CallLogItem?,
    now: Long,
    expandState: CallExpandState,
    onUpdateExpanded: (CallExpandState) -> Unit,
    actions: CallLogItemActions
) {
    val placeholderColor = MaterialTheme.colorScheme.scrim.copy(alpha = 0.5f)

    Card (
        modifier = Modifier
            .combinedClickable(
                enabled = item != null,
                onClick = {
                    onUpdateExpanded(
                        if (expandState == CallExpandState.None) CallExpandState.Basic
                        else CallExpandState.None
                    )
                },
                onLongClick = if (expandState != CallExpandState.Full) ({
                    onUpdateExpanded(CallExpandState.Full)
                }) else null
            )
            .fillMaxWidth()
            .animateItemPlacement()
    ) {
        Column (
            modifier = Modifier.padding(8.dp),
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(1.0f)
                ) {
                    if (item == null) {
                        Text(
                            "123456789",
                            color = placeholderColor,
                            modifier = Modifier.background(placeholderColor)
                        )
                    } else {
                        val number = item.number.let {
                            if (it.isEmpty()) stringResource(R.string.call_unknown_caller)
                            else it
                        }

                        if (item.contactName != null) {
                            Text(
                                item.contactName,
                                style = MaterialTheme.typography.titleLarge
                            )

                            Text(number)
                        } else {
                            Text(
                                number,
                                style = MaterialTheme.typography.titleLarge
                            )
                        }
                    }

                    if (item == null) {
                        Text(
                            "a few minutes ago - 12 minutes",
                            color = placeholderColor,
                            modifier = Modifier.background(placeholderColor)
                        )
                    } else {
                        Row {
                            Crossfade(targetState = expandState != CallExpandState.None) { isExpanded ->
                                val relativeTime =
                                    DateUtils.getRelativeTimeSpanString(
                                        item.time,
                                        now,
                                        DateUtils.MINUTE_IN_MILLIS,
                                        DateUtils.FORMAT_SHOW_DATE
                                    )

                                val absoluteTime = DateUtils.formatDateTime(
                                    LocalContext.current,
                                    item.time,
                                    DateUtils.FORMAT_SHOW_DATE or
                                            DateUtils.FORMAT_SHOW_TIME or
                                            DateUtils.FORMAT_SHOW_WEEKDAY
                                )

                                val startTime =
                                    if (isExpanded) absoluteTime
                                    else relativeTime

                                Text(startTime.toString())
                            }

                            val duration =
                                if (item.duration > 0) DateUtils.formatElapsedTime(item.duration)
                                else null

                            if (duration != null) {
                                Spacer(Modifier.weight(1.0f))
                                Text(duration, fontWeight = FontWeight.Bold)
                                Spacer(Modifier.width(16.dp))
                            }
                        }
                    }
                }

                if (item?.type != null) {
                    val tint =
                        if (item.phoneAccountColor == null || item.phoneAccountColor == 0) LocalContentColor.current
                        else Color(item.phoneAccountColor).copy(alpha = 1f)

                    Icon(
                        when (item.type) {
                            CallLogItem.Type.Incoming -> Icons.Default.CallReceived
                            CallLogItem.Type.Missed -> Icons.Default.CallMissed
                            CallLogItem.Type.Outgoing -> Icons.Default.CallMade
                            CallLogItem.Type.Mailbox -> Icons.Default.Voicemail
                            CallLogItem.Type.Blocked -> Icons.Default.Block
                            CallLogItem.Type.ExternallyAnswered -> Icons.Default.PhoneForwarded
                            CallLogItem.Type.Rejected -> Icons.Default.CallEnd
                        },
                        contentDescription = when (item.type) {
                            CallLogItem.Type.Incoming -> stringResource(R.string.call_log_type_incoming)
                            CallLogItem.Type.Missed -> stringResource(R.string.call_log_type_missed)
                            CallLogItem.Type.Outgoing -> stringResource(R.string.call_log_type_outgoing)
                            CallLogItem.Type.Mailbox -> stringResource(R.string.call_log_type_mailbox)
                            CallLogItem.Type.Blocked -> stringResource(R.string.call_log_type_block)
                            CallLogItem.Type.ExternallyAnswered -> stringResource(R.string.call_log_type_externally_answered)
                            CallLogItem.Type.Rejected -> stringResource(R.string.call_log_type_rejected)
                        },
                        modifier = Modifier
                            .size(40.dp)
                            .padding(end = 8.dp),
                        tint = tint
                    )
                }
            }

            AnimatedVisibility(visible = expandState != CallExpandState.None) {
                Column(
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .fillMaxWidth()
                ) {
                    if (item != null) {
                        Row(
                            horizontalArrangement = Arrangement.End,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            AnimatedVisibility(visible = expandState == CallExpandState.Full) {
                                TextButton(onClick = {
                                    actions.removeItem(item.id)
                                }) {
                                    Text(text = stringResource(R.string.call_log_action_delete))
                                }
                            }

                            TextButton(onClick = { actions.copyNumberToClipboard(item.number) }) {
                                Text(stringResource(R.string.call_log_action_clipboard))
                            }

                            Button(onClick = {
                                actions.dialNumber(
                                    item.number,
                                    item.phoneAccountHandle
                                )
                            }) {
                                Text(stringResource(R.string.call_log_action_call))
                            }
                        }
                    }
                }
            }
        }
    }
}

class CallLogItemActions(
    val copyNumberToClipboard: (String) -> Unit,
    val dialNumber: (String, PhoneAccountHandle?) -> Unit,
    val removeItem: (Long) -> Unit
)