/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.app.PendingIntent
import android.telecom.Call
import android.telecom.PhoneAccount
import android.telecom.PhoneAccountHandle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.compose.animation.*
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.view.children
import de.wivewa.dialer.BuildConfig
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallAudio
import de.wivewa.dialer.service.CallInfo
import de.wivewa.dialer.ui.dial.PhoneAccountDialog
import de.wivewa.dialer.ui.reflection.getOnClickListener

@Composable
fun CallScreen(
    call: CallInfo?,
    backgroundCalls: List<CallInfo>,
    audio: CallAudio?,
    onAddCall: () -> Unit,
    onShowExternalCallerDetails: (PendingIntent) -> Unit,
    executeAfterKeyguardUnlock: (() -> Unit) -> Unit,
    getPhoneAccount: (PhoneAccountHandle) -> PhoneAccount
) {
    var overlay by remember { mutableStateOf(null as InCallScreenOverlay?) }

    val (color, contentColor) =
        if (call?.phoneAccountColor == null || call.phoneAccountColor == 0)
            Pair(MaterialTheme.colorScheme.primary, MaterialTheme.colorScheme.onPrimary)
        else Pair(Color(call.phoneAccountColor).copy(alpha = 1f), Color.White)

    val heldCalls = backgroundCalls.filter { it.state == Call.STATE_HOLDING }

    Surface (
        color = color,
        contentColor = contentColor,
        modifier = Modifier.fillMaxSize()
    ) {
        Column (
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .windowInsetsPadding(WindowInsets.safeDrawing)
                .padding(16.dp, 32.dp)
        ) {
            val context = LocalContext.current

            val remoteView = remember(call?.integrationView) {
                try {
                    fun wrapClickListeners(view: View) {
                        if (view is ViewGroup) {
                            view.children.forEach { wrapClickListeners(it) }
                        }

                        view.getOnClickListener()?.let { oldListener ->
                            view.setOnClickListener { view ->
                                executeAfterKeyguardUnlock {
                                    oldListener.onClick(view)
                                }
                            }
                        }
                    }

                    call?.integrationView?.apply(context, null)?.also {
                        wrapClickListeners(it)
                    }
                } catch (ex: Exception) {
                    if (BuildConfig.DEBUG) {
                        Log.d("CallScreen", "could not setup remote view", ex)
                    }

                    null
                }
            }

            CallerInfo(call, onShowExternalCallerDetails)

            if (remoteView != null) {
                AndroidView(
                    factory = {
                        FrameLayout(it).also { frameLayout ->
                            frameLayout.layoutParams = ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            )

                            frameLayout.addView(remoteView)
                        }
                    },
                    update = { frameLayout ->
                        if (frameLayout.childCount != 1) throw IllegalStateException()

                        if (frameLayout.getChildAt(0) !== remoteView) {
                            frameLayout.removeViewAt(0)
                            frameLayout.addView(remoteView)
                        }
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .sizeIn(maxHeight = 256.dp)
                        .verticalScroll(rememberScrollState())
                )
            }

            if (
                call != null &&
                call.state != Call.STATE_RINGING &&
                call.state != Call.STATE_DISCONNECTING &&
                call.state != Call.STATE_DISCONNECTED &&
                audio != null
            ) {
                Column {
                    InCallRow {
                        val currentCallAudio = CurrentCallAudio.create(
                            audio = audio,
                            showDialog = { overlay = InCallScreenOverlay.AudioOutput },
                            context = LocalContext.current
                        )

                        InCallAction(
                            icon = Icons.Default.MicOff,
                            active = audio.isMuted,
                            label = stringResource(R.string.call_mute_on),
                            clickLabel = when (audio.isMuted) {
                                false -> stringResource(R.string.call_mute_on)
                                true -> stringResource(R.string.call_mute_off)
                            },
                            clickAction = audio.toggleMute,
                            enabled = true
                        )

                        InCallAction(
                            icon = Icons.Default.Dialpad,
                            active = overlay == InCallScreenOverlay.Dialpad,
                            label = stringResource(R.string.call_dialpad),
                            clickLabel = stringResource(R.string.call_dialpad),
                            clickAction = { overlay = InCallScreenOverlay.Dialpad },
                            enabled = call.state == Call.STATE_ACTIVE
                        )

                        InCallAction(
                            icon = currentCallAudio.icon,
                            active = currentCallAudio.active,
                            label = currentCallAudio.label,
                            clickLabel = stringResource(R.string.call_audio_options),
                            clickAction = currentCallAudio.action,
                            enabled = true
                        )
                    }

                    InCallRow {
                        if (call.canMerge) InCallAction(
                            icon = Icons.Default.CallMerge,
                            active = false,
                            label = stringResource(R.string.call_merge),
                            clickLabel = stringResource(R.string.call_merge),
                            clickAction = call.actions.merge,
                            enabled = call.state == Call.STATE_ACTIVE
                        ) else InCallAction(
                            icon = Icons.Default.Add,
                            active = false,
                            label = stringResource(R.string.call_add_call),
                            clickLabel = stringResource(R.string.call_add_call),
                            clickAction = onAddCall,
                            enabled = call.state == Call.STATE_ACTIVE
                        )

                        if (heldCalls.isEmpty()) InCallAction(
                            icon = Icons.Default.Pause,
                            active = call.state == Call.STATE_HOLDING,
                            label = stringResource(R.string.call_hold),
                            clickLabel = stringResource(R.string.call_hold),
                            clickAction = if (call.state == Call.STATE_HOLDING) call.actions.unhold else call.actions.hold,
                            enabled = call.state == Call.STATE_ACTIVE || call.state == Call.STATE_HOLDING
                        ) else {
                            val heldCall = heldCalls.singleOrNull()

                            val clickAction = heldCall?.actions?.unhold
                                ?: { overlay = InCallScreenOverlay.SelectHeldCall }

                            InCallAction(
                                icon = Icons.Default.SwapCalls,
                                active = false,
                                label = stringResource(R.string.call_swap),
                                clickLabel = stringResource(R.string.call_swap),
                                clickAction = clickAction,
                                enabled = call.state == Call.STATE_ACTIVE
                            )
                        }

                        Spacer(Modifier.weight(1.0f))
                    }
                }
            }

            CallBaseActions(call)
        }

        InCallNumberPad(
            visible = overlay == InCallScreenOverlay.Dialpad,
            playDtmfTone = call?.actions?.playDtmfTone ?: {/* ignore */},
            onDismissRequest = { overlay = null }
        )

        if (overlay == InCallScreenOverlay.AudioOutput) {
            if (audio == null) overlay = null
            else AudioRouteDialog(
                audio = audio,
                onDismissRequest = { overlay = null }
            )
        }

        if (overlay == InCallScreenOverlay.SelectHeldCall) {
            if (heldCalls.isEmpty()) overlay = null

            SelectCallDialog(
                title = stringResource(R.string.call_swap),
                calls = heldCalls,
                onCallSelected = {
                    it.actions.unhold()

                    overlay = null
                },
                onDismissRequest = { overlay = null }
            )
        }

        if (call?.state == Call.STATE_SELECT_PHONE_ACCOUNT) {
            val phoneAccounts =
                try {
                    call.phoneAccountSuggestions
                        .map { it.phoneAccountHandle }
                        .map(getPhoneAccount)
                } catch (ex: Exception) {
                    emptyList()
                }

            PhoneAccountDialog(
                currentPhoneAccount = null,
                phoneAccounts = phoneAccounts,
                selectAccount = { call.actions.selectPhoneAccount(it.accountHandle) },
                onDismissRequest = { call.actions.disconnect() }
            )
        }
    }
}

@Composable
fun InCallRow(content: @Composable RowScope.() -> Unit) {
    Row (
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Min),
        content = content
    )
}

@Composable
fun RowScope.InCallAction(
    icon: ImageVector,
    active: Boolean,
    label: String,
    clickLabel: String,
    clickAction: () -> Unit,
    enabled: Boolean
) {
    val density = LocalContext.current.resources.displayMetrics.density
    var radius by remember { mutableStateOf(16.dp) }

    val tint =
        if (active) MaterialTheme.colorScheme.primary
        else if (!enabled) LocalContentColor.current.copy(alpha = .5f)
        else LocalContentColor.current

    CompositionLocalProvider(
        LocalIndication.provides(
            ripple(
                bounded = false,
                radius = radius
            )
        )
    ) {
        Column (
            modifier = Modifier
                .fillMaxHeight()
                .weight(1.0f)
                .onSizeChanged { radius = (it.height.coerceAtMost(it.width) / (2 * density)).dp }
                .clickable(
                    enabled = enabled,
                    role = Role.Button,
                    onClickLabel = clickLabel,
                    onClick = clickAction
                )
                .padding(8.dp, 16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Crossfade(
                targetState = active,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            ) { active ->
                Surface (
                    shape = CircleShape,
                    color = if (active) MaterialTheme.colorScheme.onPrimary else Color.Unspecified
                ) {
                    Icon(
                        imageVector = icon,
                        contentDescription = label,
                        tint = tint,
                        modifier = Modifier.padding(8.dp)
                    )
                }
            }

            Text(
                label,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}

enum class InCallScreenOverlay {
    Dialpad, AudioOutput, SelectHeldCall
}