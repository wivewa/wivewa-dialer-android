/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.settings

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.wivewa.android.integration.dialer.server.configuration.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.suspendCancellableCoroutine

class ApiAccessModel(application: Application): AndroidViewModel(application) {
    private val configurationStorage = ConfigurationStorage.with(application)

    private val configuration: Flow<DialerServerConfiguration> = callbackFlow {
        val listener: ConfigurationChangeListener = { trySend(it) }

        try {
            configurationStorage.addListener(listener)

            trySend(configurationStorage.getConfig())

            suspendCancellableCoroutine {/* do nothing */}
        } finally {
            configurationStorage.removeListener(listener)
        }
    }

    private val installedApps: Flow<List<ClientApp>> = callbackFlow<List<ClientApp>> {
        val receiver = object: BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                trySend(ClientApp.fromInstalledApps(application.packageManager))
            }
        }

        try {
            application.registerReceiver(receiver, ClientApp.CHANGE_INTENT_FILTER)

            send(ClientApp.fromInstalledApps(application.packageManager))

            suspendCancellableCoroutine {/* do nothing */}
        } finally {
            application.unregisterReceiver(receiver)
        }
    }.conflate()

    val clientAppsWithState: Flow<List<ClientAppWithState>> = configuration.combine(installedApps) { configuration, installed ->
        ClientAppWithState.calculate(installed, configuration.allowedClients)
    }.shareIn(viewModelScope, SharingStarted.WhileSubscribed(1000), 1)

    fun revokeAccess(app: ClientApp) {
        configurationStorage.updateConfig { oldConfig ->
            oldConfig.copy(
                allowedClients = oldConfig.allowedClients.filterNot { it.packageName == app.packageName }
            )
        }
    }

    fun grantAccess(app: ClientApp) {
        configurationStorage.updateConfig { oldConfig ->
            oldConfig.copy(
                allowedClients = oldConfig.allowedClients.filterNot { it.packageName == app.packageName } + app
            )
        }
    }
}