/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telecom.TelecomManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.ClearAll
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.core.content.getSystemService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.work.WorkManager
import de.wivewa.dialer.R
import de.wivewa.dialer.theme.WivewaDialerTheme
import de.wivewa.dialer.ui.Common
import de.wivewa.dialer.ui.dial.MainActivity
import de.wivewa.dialer.work.ExportCallLogWorker
import kotlinx.coroutines.launch

class CallLogActivity: ComponentActivity() {
    private val model by viewModels<CallLogModel>()

    private val requestPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { model.recheckPermission() }

    private val requestExport = registerForActivityResult(
        object: ActivityResultContracts.CreateDocument("text/json") {
            override fun createIntent(context: Context, input: String): Intent =
                super.createIntent(context, input).addCategory(Intent.CATEGORY_OPENABLE)
        }
    ) { uri ->
        if (uri != null) ExportCallLogWorker.enqueue(WorkManager.getInstance(this), uri)
    }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                for (command in model.activityCommand) {
                    when (command) {
                        is CallLogModel.ActivityCommand.RequestPermission -> requestPermissions.launch(model.requiredPermissions)
                    }
                }
            }
        }

        val clipboardManager = getSystemService<ClipboardManager>()!!

        setContent {
            WivewaDialerTheme {
                val status by model.status.collectAsState(initial = null)

                var showClearDialog by rememberSaveable { mutableStateOf(false) }
                var showRemoveItemDialogForId by rememberSaveable { mutableStateOf(null as Long?) }

                val actions = remember {
                    CallLogItemActions(
                        copyNumberToClipboard = {
                            clipboardManager.setPrimaryClip(
                                ClipData.newPlainText(
                                    getString(R.string.call_log_action_clipboard_data_type),
                                    it
                                )
                            )
                        },
                        dialNumber = { number, handle ->
                            startActivity(
                                Intent(Intent.ACTION_CALL, Uri.fromParts("tel", number, null))
                                    .putExtra(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, handle)
                                    .setPackage(packageName)
                            )
                        },
                        removeItem = {
                            showRemoveItemDialogForId = it
                        }
                    )
                }

                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(stringResource(R.string.call_log_title))
                            },
                            navigationIcon = {
                                IconButton(onClick = {
                                    startActivity(
                                        Intent(this, MainActivity::class.java)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    )
                                }) {
                                    Icon(
                                        Icons.AutoMirrored.Filled.ArrowBack,
                                        stringResource(R.string.generic_back),
                                        tint = MaterialTheme.colorScheme.onPrimary
                                    )
                                }
                            },
                            actions = {
                                AnimatedVisibility(visible = status is CallLogModel.Status.Ready) {
                                    IconButton(onClick = { showClearDialog = true }) {
                                        Icon(
                                            Icons.Default.ClearAll,
                                            stringResource(R.string.call_log_clear_title)
                                        )
                                    }
                                }

                                AnimatedVisibility(visible = status is CallLogModel.Status.Ready) {
                                    IconButton(onClick = {
                                        requestExport.launch("welefon-call-log.json")
                                    }) {
                                        Icon(
                                            Icons.Default.Save,
                                            stringResource(R.string.call_log_export_title)
                                        )
                                    }
                                }
                            },
                            colors = Common.topAppBarColors()
                        )
                    }
                ) { padding ->
                    val nowLive by model.now.collectAsState(initial = null)
                    val list = model.pager.collectAsLazyPagingItems()
                    val expendedCall by model.expandedCall.collectAsState(initial = null)

                    val now = nowLive

                    if (now != null) when (val screen = status) {
                        is CallLogModel.Status.MissingPermission -> MissingCallLogPermission(
                            requestPermission = screen.requestPermission,
                            modifier = Modifier.padding(padding)
                        )
                        CallLogModel.Status.Empty -> EmptyCallLog(modifier = Modifier.padding(padding))
                        is CallLogModel.Status.Ready -> CallLogList(
                            list = list,
                            now = now,
                            expandedCall = expendedCall,
                            updateExpandedCall = { model.expandedCall.value = it },
                            actions = actions,
                            contentPadding = padding
                        )
                        null -> {/* nothing to do */}
                    }
                }

                if (showClearDialog) ClearCallLogDialog(
                    onDismissRequest = { showClearDialog = false },
                    onTriggerDeletion = { model.wipeData() }
                ) else if (showRemoveItemDialogForId != null) RemoveCallLogItemDialog(
                    onDismiss = { showRemoveItemDialogForId = null },
                    onConfirm = {
                        model.removeCallLogItem(showRemoveItemDialogForId!!)

                        showRemoveItemDialogForId = null
                    }
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()

        model.recheckPermission()
        model.reportCallsRead()
    }
}