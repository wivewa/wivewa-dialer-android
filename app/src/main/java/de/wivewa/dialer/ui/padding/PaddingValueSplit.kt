/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.padding

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

data class PaddingValueSplit (
    val paddingValues: PaddingValues,
    val top: Boolean,
    val bottom: Boolean,
    val left: Boolean,
    val right: Boolean
): PaddingValues {
    override fun calculateLeftPadding(layoutDirection: LayoutDirection): Dp =
        if (left) paddingValues.calculateLeftPadding(layoutDirection)
        else 0.dp

    override fun calculateTopPadding(): Dp =
        if (top) paddingValues.calculateTopPadding()
        else 0.dp

    override fun calculateRightPadding(layoutDirection: LayoutDirection): Dp =
        if (right) paddingValues.calculateRightPadding(layoutDirection)
        else 0.dp

    override fun calculateBottomPadding(): Dp =
        if (bottom) paddingValues.calculateBottomPadding()
        else 0.dp
}