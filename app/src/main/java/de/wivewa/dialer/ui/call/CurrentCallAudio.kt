/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.content.Context
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.VolumeUp
import androidx.compose.ui.graphics.vector.ImageVector
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallAudio

data class CurrentCallAudio(
    val icon: ImageVector,
    val label: String,
    val active: Boolean,
    val action: () -> Unit
) {
    companion object {
        fun create(
            audio: CallAudio,
            showDialog: () -> Unit,
            context: Context
        ): CurrentCallAudio {
            val hasBuiltin = audio.availableRoutes.any {
                it is CallAudio.Route.Earpiece || it is CallAudio.Route.WiredHeadsetOrEarpiece
            }

            val isActive = hasBuiltin && audio.availableRoutes.size > 1 && audio.currentRoutes.none {
                it is CallAudio.Route.Earpiece || it is CallAudio.Route.WiredHeadsetOrEarpiece
            }

            val currentRoute = audio.currentRoutes.singleOrNull()
            val currentRouteIsDefault =
                currentRoute is CallAudio.Route.Earpiece ||
                        currentRoute is CallAudio.Route.WiredHeadset ||
                        currentRoute is CallAudio.Route.WiredHeadsetOrEarpiece

            val alternativeRoute = (audio.availableRoutes - audio.currentRoutes).singleOrNull()

            val currentAudioRoute =
                if (alternativeRoute is CallAudio.Route.Speaker && currentRouteIsDefault) alternativeRoute
                else currentRoute

            val currentRoutePresentation = currentAudioRoute?.let {
                CallAudioPresentation.forRoute(it, context)
            }

            val otherRoute = (audio.availableRoutes - audio.currentRoutes.toSet()).singleOrNull()

            val icon = currentRoutePresentation?.icon ?: Icons.Default.VolumeUp
            val label = currentRoutePresentation?.shortText ?: context.getString(R.string.call_audio_options)

            val action = if (otherRoute != null) ({ audio.useAudioRoute(otherRoute) })
            else showDialog

            return CurrentCallAudio(
                icon = icon,
                label = label,
                active = isActive,
                action = action
            )
        }
    }
}