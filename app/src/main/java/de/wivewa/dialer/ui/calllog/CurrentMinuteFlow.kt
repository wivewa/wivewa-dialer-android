/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

object CurrentMinuteFlow {
    fun with(context: Context): Flow<Long> = callbackFlow {
        fun update() {
            trySend(System.currentTimeMillis()).getOrThrow()
        }

        val receiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                update()
            }
        }

        context.registerReceiver(receiver, IntentFilter(Intent.ACTION_DATE_CHANGED))
        context.registerReceiver(receiver, IntentFilter(Intent.ACTION_TIME_CHANGED))
        context.registerReceiver(receiver, IntentFilter(Intent.ACTION_TIME_TICK))

        update()

        awaitClose { context.unregisterReceiver(receiver) }
    }
}