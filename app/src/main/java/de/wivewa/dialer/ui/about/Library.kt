/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.about

data class Library (
    val title: String,
    val libraryUrl: String?,
    val licenseUrl: String?,
    val license: License
) {
    companion object {
        val KOTLIN_STANDARD_LIBRARY = Library(
            title = "Kotlin Standard Library",
            libraryUrl = "https://kotlinlang.org/api/latest/jvm/stdlib/",
            licenseUrl = "https://github.com/JetBrains/kotlin/blob/master/license/LICENSE.txt",
            license = License.APACHE_2_0
        )

        val ANDROIX_LIBRARIES = Library(
            title = "AndroidX",
            libraryUrl = "https://developer.android.com/jetpack/androidx",
            licenseUrl = null,
            license = License.APACHE_2_0
        )
    }
}