/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.os.Handler
import android.os.Looper
import android.provider.CallLog
import android.telecom.TelecomManager
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch

@SuppressLint("MissingPermission")
class CallLogModel(application: Application): AndroidViewModel(application) {
    companion object {
        private val handler = Handler(Looper.getMainLooper())
    }

    sealed class Status {
        object Ready: Status()
        object Empty: Status()
        class MissingPermission(val requestPermission: () -> Unit): Status()
    }

    sealed class ActivityCommand {
        object RequestPermission: ActivityCommand()
    }

    private val telecomManager = application.getSystemService<TelecomManager>()!!
    private val activeSourceLock = Object()
    private val activeSources: MutableList<CallLogPagingSource> = mutableListOf()
    private val recheckPermission = Channel<Unit>()
    private val statusInternal = MutableStateFlow(null as Status?)
    private val activityCommandInternal = Channel<ActivityCommand>()

    private fun invalidate() {
        synchronized(activeSourceLock) {
            activeSources.toList().also {
                activeSources.clear()
            }
        }.forEach { it.invalidate() }
    }

    private fun hasPermission(): Boolean = requiredPermissions.all {
        ContextCompat.checkSelfPermission(getApplication(), it) == PackageManager.PERMISSION_GRANTED
    }

    val status: Flow<Status?> = statusInternal
    val activityCommand: Channel<ActivityCommand> = activityCommandInternal
    val expandedCall = MutableStateFlow(null as ExpandedCall?)

    val now = CurrentMinuteFlow.with(application).buffer(1)

    val requiredPermissions = arrayOf(
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.WRITE_CALL_LOG
    )

    val pager = Pager(
        config = PagingConfig(25),
        pagingSourceFactory = {
            CallLogPagingSource(application).also { new ->
                synchronized(activeSourceLock) {
                    activeSources += new
                }
            }
        }
    ).flow.cachedIn(viewModelScope)

    fun reportCallsRead() {
        if (telecomManager.defaultDialerPackage == getApplication<Application>().packageName) {
            telecomManager.cancelMissedCallsNotification()
        }
    }

    fun recheckPermission() { recheckPermission.trySend(Unit) }

    fun wipeData() {
        viewModelScope.launch {
            if (hasPermission()) {
                Dispatchers.IO.invoke {
                    getApplication<Application>().contentResolver.delete(
                        CallLog.Calls.CONTENT_URI, null, null
                    )
                }
            }
        }
    }

    fun removeCallLogItem(id: Long) {
        viewModelScope.launch {
            if (hasPermission()) {
                Dispatchers.IO.invoke {
                    getApplication<Application>().contentResolver.delete(
                        CallLog.Calls.CONTENT_URI,
                        "${CallLog.Calls._ID} = ?",
                        arrayOf(id.toString())
                    )
                }
            }
        }
    }

    init {
        viewModelScope.launch {
            val requestPermission: () -> Unit = { activityCommandInternal.trySend(ActivityCommand.RequestPermission) }
            val changeNotificationChannel = Channel<Unit>(Channel.CONFLATED)

            if (!hasPermission()) {
                do {
                    statusInternal.value = Status.MissingPermission(requestPermission)

                    recheckPermission.receive()
                } while (!hasPermission())

                invalidate()
            }

            val observer = object: ContentObserver(handler) {
                override fun onChange(selfChange: Boolean) {
                    super.onChange(selfChange)

                    changeNotificationChannel.trySend(Unit)
                }
            }

            try {
                application.contentResolver.registerContentObserver(
                    CallLog.Calls.CONTENT_URI,
                    true,
                    observer
                )

                while (true) {
                    val hasData = Dispatchers.IO.invoke {
                        getApplication<Application>().contentResolver.query(
                            CallLog.Calls.CONTENT_URI.buildUpon()
                                .appendQueryParameter(CallLog.Calls.LIMIT_PARAM_KEY, "1")
                                .build(),
                            null, null, null, null
                        )?.use { it.moveToFirst() } ?: false
                    }

                    if (hasData) statusInternal.value = Status.Ready
                    else statusInternal.value = Status.Empty

                    changeNotificationChannel.receive()
                    invalidate()
                }
            } finally {
                application.contentResolver.unregisterContentObserver(observer)
            }
        }
    }
}