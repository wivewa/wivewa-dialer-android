/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.apirequest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import de.wivewa.android.integration.dialer.server.configuration.ClientApp
import de.wivewa.android.integration.dialer.server.configuration.ConfigurationStorage
import de.wivewa.dialer.R

class ApiRequestActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val configurationStorage = ConfigurationStorage.with(this)
        val requestingApp = callingPackage?.let { ClientApp.fromPackage(packageManager, it) }

        if (requestingApp == null) {
            setResult(RESULT_CANCELED)
            finish()

            return
        }

        if (configurationStorage.getConfig().allowedClients.any { it.doesMatchInstalledApp(requestingApp) }) {
            setResult(RESULT_OK)
            finish()

            return
        }

        val accept = {
            configurationStorage.updateConfig { oldConfig ->
                oldConfig.copy(allowedClients = oldConfig.allowedClients.filterNot { it.packageName == requestingApp.packageName } + requestingApp)
            }

            setResult(RESULT_OK)
            finish()
        }

        val deny = {
            setResult(RESULT_CANCELED)
            finish()
        }

        setContent {
            Dialog(onDismissRequest = deny) {
                Surface (
                    shape = MaterialTheme.shapes.medium
                ) {
                    Column {
                        Column(
                            modifier = Modifier.padding(16.dp),
                            verticalArrangement = Arrangement.spacedBy(8.dp)
                        ) {
                            Text(
                                buildAnnotatedString {
                                    append(stringResource(R.string.api_request_text_prefix))
                                    append(' ')
                                    withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) { append(requestingApp.title) }
                                    append(' ')
                                    append(stringResource(R.string.api_request_text_suffix))
                                },
                                textAlign = TextAlign.Center,
                                style = MaterialTheme.typography.bodyLarge
                            )

                            Text(
                                stringResource(R.string.api_request_info),
                                textAlign = TextAlign.Center,
                                style = MaterialTheme.typography.bodySmall
                            )
                        }

                        Divider()

                        TextButton(
                            onClick = accept,
                            modifier = Modifier.fillMaxWidth(),
                            shape = RectangleShape
                        ) {
                            Text(
                                stringResource(R.string.api_request_allow),
                                modifier = Modifier.padding(4.dp)
                            )
                        }

                        Divider()

                        TextButton(
                            onClick = deny,
                            modifier = Modifier.fillMaxWidth(),
                            shape = RectangleShape
                        ) {
                            Text(
                                stringResource(R.string.api_request_deny),
                                modifier = Modifier.padding(4.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}