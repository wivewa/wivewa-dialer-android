/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.dial

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout

@Composable
fun MinHeightToSecondViewColumn(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Layout(content = content, modifier = modifier) {measurables, constraints ->
        if (measurables.size != 2) throw IllegalStateException()

        val size1 = measurables[0].measure(constraints.copy(minHeight = 0, minWidth = 0))

        val size2 = measurables[1].measure(constraints.copy(
            minHeight = (constraints.minHeight - size1.measuredHeight)
                .coerceAtLeast(0),
            minWidth = 0
        ))

        val sizes = listOf(size1, size2)
        val maxWidth = sizes.maxOf { it.measuredWidth }
        val height = sizes.map { it.measuredHeight }.sum()

        layout(maxWidth, height) {
            var y = 0

            sizes.forEach {
                it.place((maxWidth - it.width) / 2, y)

                y += it.measuredHeight
            }
        }
    }
}