/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.reflection

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import de.wivewa.dialer.BuildConfig
import java.lang.reflect.Field
import java.lang.reflect.Method

fun View.getOnClickListener(): OnClickListener? =
    if (hasOnClickListeners()) OnClickListenerReflectionData.instance?.execute(this)
    else null

internal data class OnClickListenerReflectionData(
    val getListenerInfoMethod: Method,
    val clickListenerField: Field
) {
    companion object {
        private const val LOG_TAG = "OnClickListenerReflectionData"

        val instance by lazy {
            try {
                create()
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.d(LOG_TAG, "not possible", ex)
                }

                null
            }
        }

        @SuppressLint("DiscouragedPrivateApi", "PrivateApi")
        internal fun create(): OnClickListenerReflectionData {
            val getListenerInfoMethod = View::class.java.getDeclaredMethod("getListenerInfo")
            val listenerInfoClass = Class.forName("android.view.View\$ListenerInfo")
            val clickListenerField = listenerInfoClass.getField("mOnClickListener")

            getListenerInfoMethod.isAccessible = true

            return OnClickListenerReflectionData(
                getListenerInfoMethod = getListenerInfoMethod,
                clickListenerField = clickListenerField
            )
        }
    }

    internal fun execute(view: View): OnClickListener? {
        val listenerInfo = getListenerInfoMethod.invoke(view)
        val listener = clickListenerField.get(listenerInfo)

        return if (listener is OnClickListener) listener
        else null
    }
}