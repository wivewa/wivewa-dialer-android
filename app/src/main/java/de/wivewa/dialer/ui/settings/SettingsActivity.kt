/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.settings

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.R
import de.wivewa.dialer.data.AppSettings
import de.wivewa.dialer.theme.WivewaDialerTheme
import de.wivewa.dialer.ui.Common
import de.wivewa.dialer.ui.about.AboutCard
import de.wivewa.dialer.ui.about.AppInfo

class SettingsActivity: ComponentActivity() {
    private val model by viewModels<ApiAccessModel>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val about = AppInfo.create(this)
        val settings = AppSettings.with(application)

        setContent {
            WivewaDialerTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = { Text(stringResource(R.string.settings_title)) },
                            colors = Common.topAppBarColors()
                        )
                    }
                ) { padding ->
                    Column(
                        modifier = Modifier
                            .verticalScroll(rememberScrollState())
                            .fillMaxSize()
                            .padding(padding)
                            .padding(8.dp),
                        verticalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        val apps by model.clientAppsWithState.collectAsState(initial = emptyList())

                        ApiAccessCard(
                            apps = apps,
                            grantAccess = { model.grantAccess(it) },
                            revokeAccess = { model.revokeAccess(it) }
                        )

                        AutoAudioCard(settings = settings)

                        AboutCard(info = about)
                    }
                }
            }
        }
    }
}