/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.dial

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver.OnPreDrawListener
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Contacts
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.core.view.WindowCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.wivewa.dialer.R
import de.wivewa.dialer.theme.WivewaDialerTheme
import de.wivewa.dialer.ui.Common
import de.wivewa.dialer.ui.call.CallActivity
import de.wivewa.dialer.ui.calllog.CallLogActivity
import de.wivewa.dialer.ui.settings.SettingsActivity
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val model by viewModels<MainModel>()

    private val requestRole = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            model.retryHasRole()
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) model.handleIntent(intent)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                for (command in model.activityCommand) {
                    when (command) {
                        MainModel.ActivityCommand.RequestRole -> requestRole.launch(model.requestRoleIntent)
                        MainModel.ActivityCommand.LaunchCallScreen -> startActivity(Intent(this@MainActivity, CallActivity::class.java))
                        MainModel.ActivityCommand.LaunchContacts -> try {
                            startActivity(
                                Intent()
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .setComponent(Contacts.component)
                            )
                        } catch (ex: ActivityNotFoundException) {
                            Toast.makeText(this@MainActivity, R.string.contacts_error_toast, Toast.LENGTH_SHORT).show()
                        }
                        MainModel.ActivityCommand.LaunchCallLog -> launchCallLog()
                    }
                }
            }
        }

        lifecycleScope.launch {
            val content = findViewById<View>(android.R.id.content)
            val blockDrawing = OnPreDrawListener { false }

            content.viewTreeObserver.addOnPreDrawListener(blockDrawing)
            model.status.firstOrNull()
            content.viewTreeObserver.removeOnPreDrawListener(blockDrawing)
        }

        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            val statusLive by model.status.collectAsState(null)

            WivewaDialerTheme {
                Scaffold(
                    contentWindowInsets = WindowInsets.safeDrawing,
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(stringResource(R.string.app_name))
                            },
                            colors = Common.topAppBarColors(),
                            actions = {
                                val launchContacts = statusLive.let {
                                    when (it) {
                                        is MainModel.State.Dialpad -> it.launchContacts
                                        else -> null
                                    }
                                }

                                if (launchContacts != null) {
                                    IconButton(onClick = launchContacts) {
                                        Icon(
                                            Icons.Default.Contacts,
                                            stringResource(R.string.contacts_title)
                                        )
                                    }
                                }

                                IconButton(onClick = ::launchSettings) {
                                    Icon(
                                        Icons.Default.Settings,
                                        stringResource(R.string.settings_title)
                                    )
                                }
                            }
                        )
                    }
                ) { padding ->
                    when (val status = statusLive) {
                        is MainModel.State.MissingRole -> RoleScreen(
                            status.request,
                            Modifier
                                .padding(padding)
                                .fillMaxSize()
                        )
                        is MainModel.State.Dialpad -> DialScreen(status, padding)
                        null -> {/* nothing to do */ }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        model.retryHasRole()
    }

    private fun launchCallLog() {
        startActivity(Intent(this, CallLogActivity::class.java))
    }

    private fun launchSettings() {
        startActivity(Intent(this, SettingsActivity::class.java))
    }
}