/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.demo

import android.content.Intent
import android.os.Bundle
import android.telecom.Call
import android.telecom.TelecomManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.demo.about.AboutActivity
import de.wivewa.dialer.demo.ui.theme.AppTheme

class MainActivity : ComponentActivity() {
    private val model by viewModels<MainModel>()

    private val requestPermission = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) model.update()
    }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            AppTheme {
                Scaffold(
                    snackbarHost = { SnackbarHost(model.snackbar) },
                    topBar = {
                        TopAppBar(
                            title = { Text(stringResource(R.string.app_name)) },
                            colors = TopAppBarDefaults.topAppBarColors(
                                containerColor = MaterialTheme.colorScheme.primaryContainer
                            ),
                            actions = {
                                IconButton(
                                    onClick = {
                                        startActivity(Intent(this@MainActivity, AboutActivity::class.java))
                                    }
                                ) {
                                    Icon(
                                        Icons.Outlined.Info,
                                        stringResource(R.string.about_title)
                                    )
                                }
                            }
                        )
                    }
                ) { padding ->
                    val stateLive by model.state.collectAsState(initial = null)

                    when (val state = stateLive) {
                        MainModel.State.CommunicationError -> CenteredText(
                            stringResource(R.string.communication_error),
                            padding
                        )
                        MainModel.State.UnsupportedDialer -> CenteredText(
                            stringResource(R.string.unsupported_dialer),
                            padding
                        )
                        MainModel.State.MissingPermission -> Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(padding)
                                .padding(16.dp),
                            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically)
                        ) {
                            Text(
                                stringResource(R.string.missing_permission),
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center
                            )

                            Button(
                                modifier = Modifier.align(Alignment.CenterHorizontally),
                                onClick = {
                                    requestPermission.launch(model.requestPermissionIntent)
                                }
                            ) {
                                Text(stringResource(R.string.request_permission))
                            }
                        }
                        null -> Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(padding)
                                .padding(16.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            CircularProgressIndicator()
                        }
                        is MainModel.State.Connected -> {
                            if (state.calls.isEmpty())
                                CenteredText(stringResource(R.string.no_calls), padding)
                            else
                                LazyColumn(
                                    contentPadding = PaddingValueSum(
                                        padding,
                                        PaddingValues(8.dp)
                                    ),
                                    verticalArrangement = Arrangement.spacedBy(8.dp)
                                ) {
                                    items (state.calls, key = { it.id }) { call ->
                                        Card(
                                            modifier = Modifier.fillMaxWidth()
                                        ) {
                                            Column(
                                                modifier = Modifier.padding(8.dp),
                                                verticalArrangement = Arrangement.spacedBy(8.dp)
                                            ) {
                                                Text(stringResource(R.string.call_id, call.id))

                                                Text(stringResource(
                                                    R.string.call_remote_handle,
                                                    call.remoteHandle,
                                                    when (call.handlePresentation) {
                                                        TelecomManager.PRESENTATION_ALLOWED -> "allowed"
                                                        TelecomManager.PRESENTATION_PAYPHONE -> "payphone"
                                                        TelecomManager.PRESENTATION_RESTRICTED -> "restricted"
                                                        TelecomManager.PRESENTATION_UNKNOWN -> "unknown"
                                                        TelecomManager.PRESENTATION_UNAVAILABLE -> "unavailable"
                                                        else -> call.handlePresentation
                                                    }
                                                ))

                                                Text(stringResource(
                                                    R.string.call_state,
                                                    when (call.state) {
                                                        Call.STATE_ACTIVE -> "active"
                                                        Call.STATE_AUDIO_PROCESSING -> "audio processing"
                                                        Call.STATE_CONNECTING -> "connecting"
                                                        Call.STATE_DIALING -> "dialing"
                                                        Call.STATE_DISCONNECTED -> "disconnected"
                                                        Call.STATE_DISCONNECTING -> "disconnecting"
                                                        Call.STATE_HOLDING -> "holding"
                                                        Call.STATE_NEW -> "new"
                                                        Call.STATE_PULLING_CALL -> "pulling call"
                                                        Call.STATE_RINGING -> "ringing"
                                                        Call.STATE_SELECT_PHONE_ACCOUNT -> "select phone account"
                                                        Call.STATE_SIMULATED_RINGING -> "simulated ringing"
                                                        else -> call.state
                                                    }
                                                ))

                                                Text(stringResource(
                                                    R.string.call_direction,
                                                    when (call.direction) {
                                                        Call.Details.DIRECTION_INCOMING -> "incoming"
                                                        Call.Details.DIRECTION_OUTGOING -> "outgoing"
                                                        Call.Details.DIRECTION_UNKNOWN -> "unknown"
                                                        else -> call.direction
                                                    }
                                                ))

                                                Text(stringResource(
                                                    R.string.phone_account,
                                                    listOf(call.phoneAccount.id, call.phoneAccount.componentName.toShortString())
                                                        .joinToString(separator = "@")
                                                ))

                                                TextButton(onClick = { model.assignName(call) }) {
                                                    Text(stringResource(R.string.action_set_name))
                                                }

                                                TextButton(onClick = { model.assignCallerAction(call) }) {
                                                    Text(stringResource(R.string.action_set_caller_action))
                                                }

                                                TextButton(onClick = { model.assignView(call, R.layout.remote_textview) }) {
                                                    Text(stringResource(R.string.action_view_text))
                                                }

                                                TextButton(onClick = { model.assignView(call, R.layout.remote_longview) }) {
                                                    Text(stringResource(R.string.action_view_long))
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        model.update()
    }
}

@Composable
fun CenteredText(
    text: String,
    padding: PaddingValues
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(padding)
            .padding(16.dp),
        contentAlignment = Alignment.Center
    ) {
        Text(text)
    }
}