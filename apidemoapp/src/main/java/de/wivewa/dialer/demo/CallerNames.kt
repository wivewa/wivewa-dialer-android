/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.demo

import kotlin.random.Random

object CallerNames {
    private val ITEMS = listOf(
        "Lucie Barker",
        "Harmony Hall",
        "Ajay Wolf",
        "Isabelle Dickerson",
        "Freddie Winters",
        "Maya Ball",
        "Kye Erickson",
        "Byron Ryan"
    )

    fun pick() = ITEMS[Random.nextInt(ITEMS.size)]
}