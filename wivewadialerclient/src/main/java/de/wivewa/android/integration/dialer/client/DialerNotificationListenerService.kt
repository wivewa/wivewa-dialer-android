/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.client

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.telecom.TelecomManager
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.WivewaDialerListener

abstract class DialerNotificationListenerService: Service() {
    companion object {
        private val handler = Handler(Looper.getMainLooper())
    }

    private lateinit var client: SimpleDialerClient
    private lateinit var telecomManager: TelecomManager

    override fun onCreate() {
        super.onCreate()

        client = SimpleDialerClient(this, handler)
        telecomManager = getSystemService(Context.TELECOM_SERVICE) as TelecomManager

        client.start()
    }

    override fun onDestroy() {
        super.onDestroy()

        client.stop()
    }

    override fun onBind(p0: Intent?): IBinder = object: WivewaDialerListener.Stub() {
        private fun checkCaller() {
            val dialer = telecomManager.defaultDialerPackage
            val caller = packageManager.getNameForUid(Binder.getCallingUid())

            if (dialer == null || caller != dialer) {
                throw SecurityException()
            }
        }

        override fun onCallUpdated(callInfo: CallInfo) {
            checkCaller()

            this@DialerNotificationListenerService.onCallUpdated(callInfo)
        }

        override fun onCallEnded(callInfo: CallInfo) {
            checkCaller()

            this@DialerNotificationListenerService.onCallEnded(callInfo)
        }
    }

    abstract fun onCallUpdated(callInfo: CallInfo)
    abstract fun onCallEnded(callInfo: CallInfo)

    fun enhanceCall(callInfo: CallInfo, extras: CallExtras) {
        client.enhanceCall(callInfo.id, extras) {/* ignore result */}
    }

    fun getCurrentCalls(callback: (Result<List<CallInfo>>) -> Unit) {
        client.getCurrentCalls(callback)
    }
}