/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer;

public abstract class DialerIntegrationActions {
    private DialerIntegrationActions() {}

    // action for a service that implements WivewaDialerService
    public static final String ACTION_DIALER_SERVICE = "de.wivewa.android.integration.dialer.service.DIALER";

    // action for a service that implements WivewaDialerListener
    public static final String ACTION_LISTENER_SERVICE = "de.wivewa.android.integration.dialer.service.LISTENER";

    // action for a activity that can grant the permission
    // this activity uses the caller package naem and thus should be called with startActivityForResult
    // returns Activity.RESULT_OK if and only if the permission was granted
    public static final String ACTION_REQUEST_PERMISSION = "de.wivewa.android.integration.dialer.activity.REQUEST_PERMISSION";
}
