/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.configuration

import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.util.Base64
import android.util.JsonReader
import android.util.JsonWriter
import de.wivewa.android.integration.dialer.DialerIntegrationActions
import de.wivewa.android.integration.dialer.server.extensions.queryIntentServices

data class ClientApp(
    val title: String,
    val packageName: String,
    val signatures: List<ByteArray>,
    val multiSigner: Boolean
) {
    companion object {
        private const val TITLE = "title"
        private const val PACKAGE_NAME = "packageName"
        private const val SIGNATURES = "signatures"
        private const val MULTI_SIGNER = "multiSigner"

        private val BROADCAST_ACTIONS = listOf(
            Intent.ACTION_PACKAGE_ADDED,
            Intent.ACTION_PACKAGE_REMOVED,
            Intent.ACTION_PACKAGE_REPLACED,
            Intent.ACTION_PACKAGE_CHANGED
        )

        val CHANGE_INTENT_FILTER = IntentFilter().also { filter ->
            BROADCAST_ACTIONS.forEach { action -> filter.addAction(action) }

            filter.addDataScheme("package")
        }

        fun fromUid(uid: Int, packageManager: PackageManager) =
            fromPackage(packageManager, packageManager.getNameForUid(uid)!!)

        fun fromPackage(packageManager: PackageManager, packageName: String): ClientApp {
            val info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNING_CERTIFICATES
            )

            val title = info.applicationInfo.loadLabel(packageManager).toString()

            return if (info.signingInfo.hasMultipleSigners()) {
                ClientApp(
                    title = title,
                    packageName = packageName,
                    signatures = info.signingInfo.apkContentsSigners.map { it.toByteArray() },
                    multiSigner = true
                )
            } else {
                ClientApp(
                    title = title,
                    packageName = packageName,
                    signatures = info.signingInfo.signingCertificateHistory.map { it.toByteArray() },
                    multiSigner = false
                )
            }
        }

        fun fromInstalledApps(packageManager: PackageManager): List<ClientApp> =
            packageManager.queryIntentServices(Intent(DialerIntegrationActions.ACTION_LISTENER_SERVICE))
                .map { it.serviceInfo.packageName }
                .toSet()
                .map { fromPackage(packageManager, it) }

        fun parse(reader: JsonReader): ClientApp {
            var title: String? = null
            var packageName: String? = null
            var signatures: List<ByteArray>? = null
            var multiSigner: Boolean? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    TITLE -> title = reader.nextString()
                    PACKAGE_NAME -> packageName = reader.nextString()
                    SIGNATURES -> signatures = mutableListOf<ByteArray>().also { list ->
                        reader.beginArray()
                        while (reader.hasNext()) {
                            list.add(Base64.decode(reader.nextString(), Base64.NO_WRAP))
                        }
                        reader.endArray()
                    }
                    MULTI_SIGNER -> multiSigner = reader.nextBoolean()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return ClientApp(
                title = title ?: packageName!!,
                packageName = packageName!!,
                signatures = signatures!!,
                multiSigner = multiSigner!!
            )
        }
    }

    fun doesMatchInstalledApp(installedApp: ClientApp): Boolean {
        if (this.packageName != installedApp.packageName) return false

        if (this.multiSigner) {
            return this.signatures.all { signature ->
                installedApp.signatures.find { it.contentEquals(signature) } != null
            }
        } else {
            return this.signatures.find { signature ->
                installedApp.signatures.find { it.contentEquals(signature) } != null
            } != null
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(TITLE).value(title)
        writer.name(PACKAGE_NAME).value(packageName)
        writer.name(MULTI_SIGNER).value(multiSigner)

        writer.name(SIGNATURES).beginArray()
        signatures.forEach { writer.value(Base64.encodeToString(it, Base64.NO_WRAP)) }
        writer.endArray()

        writer.endObject()
    }
}