/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.WivewaDialerServer
import de.wivewa.android.integration.dialer.server.configuration.ClientApp
import de.wivewa.android.integration.dialer.server.configuration.ConfigurationStorage
import de.wivewa.android.integration.dialer.server.extensions.BinderInfo
import de.wivewa.android.integration.dialer.server.extensions.switchBinderIdentity

class WivewaDialerService : Service() {
    override fun onBind(intent: Intent): IBinder = object: WivewaDialerServer.Stub() {
        private val configurationStorage = ConfigurationStorage.with(this@WivewaDialerService)

        private fun wasPermissionGranted(clientApp: ClientApp) = configurationStorage.getConfig()
            .allowedClients.find { it.doesMatchInstalledApp(clientApp) } != null

        private fun wasPermissionGranted(binder: BinderInfo) = wasPermissionGranted(ClientApp.fromUid(binder.uid, packageManager))

        private fun assertPermissionGranted(binder: BinderInfo) {
            if (!wasPermissionGranted(binder)) throw SecurityException()
        }

        override fun wasPermissionGranted(): Boolean = switchBinderIdentity { binder ->
            wasPermissionGranted(binder)
        }

        override fun getCurrentCalls(): MutableList<CallInfo> = switchBinderIdentity { binder ->
            assertPermissionGranted(binder)

            ConnectedInCallService.getCalls().values.toMutableList()
        }

        override fun enhanceCall(callId: String, extras: CallExtras) = switchBinderIdentity { binder ->
            assertPermissionGranted(binder)

            ConnectedInCallService.setExtras(callId, extras)
        }
    }.asBinder()
}
