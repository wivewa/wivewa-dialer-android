/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.service

import android.telecom.Call
import android.telecom.InCallService
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.server.helper.CallInfoManager
import de.wivewa.android.integration.dialer.server.helper.ListenerConnectionManager
import java.util.UUID

abstract class ConnectedInCallService: InCallService() {
    companion object {
        private var instance: ConnectedInCallService? = null

        fun getCalls(): Map<String, CallInfo> = instance?.calls?.mapValues { it.value.callInfo } ?: emptyMap()

        fun setExtras(callId: String, extras: CallExtras) {
            instance?.also { instance ->
                val call = synchronized(instance) { instance.calls[callId]?.call }

                if (call != null) instance.onCallExtrasUpdated(call, extras)
            }
        }
    }

    private lateinit var listenerManager: ListenerConnectionManager
    private var calls: Map<String, CallMeta> = emptyMap()

    internal data class CallMeta(val call: Call, val callInfo: CallInfo)

    override fun onCreate() {
        super.onCreate()

        listenerManager = ListenerConnectionManager(this)
        instance = this

        listenerManager.start()
    }

    override fun onDestroy() {
        super.onDestroy()

        instance = null

        listenerManager.stop()
    }

    override fun onCallAdded(call: Call) {
        super.onCallAdded(call)

        val callId = UUID.randomUUID().toString()

        kotlin.run {
            val info = CallInfoManager.getInfo(call, callId)

            synchronized(this) {
                calls = calls + Pair(info.id, CallMeta(call, info))
                listenerManager.send(ListenerConnectionManager.Message.CallUpdated(info))
            }
        }

        call.registerCallback(object: Call.Callback() {
            override fun onStateChanged(call: Call, state: Int) {
                super.onStateChanged(call, state)

                synchronized(this) {
                    val info = CallInfoManager.getInfo(call, callId)

                    calls = calls + Pair(
                        info.id,
                        calls[info.id]?.copy(call = call, callInfo = info) ?:
                        CallMeta(call, info)
                    )

                    listenerManager.send(ListenerConnectionManager.Message.CallUpdated(info))
                }
            }

            override fun onDetailsChanged(call: Call, details: Call.Details) {
                super.onDetailsChanged(call, details)

                synchronized(this) {
                    val info = CallInfoManager.getInfo(call, callId)

                    calls = calls + Pair(
                        info.id,
                        calls[info.id]?.copy(call = call, callInfo = info) ?:
                        CallMeta(call, info)
                    )

                    listenerManager.send(ListenerConnectionManager.Message.CallUpdated(info))
                }
            }
        })
    }

    override fun onCallRemoved(call: Call) {
        super.onCallRemoved(call)

        synchronized(this) {
            val callMetadata = calls.values.find { it.call === call } ?: return

            val info = CallInfoManager.getInfo(call, callMetadata.callInfo.id)

            calls = calls - info.id
            listenerManager.send(ListenerConnectionManager.Message.CallDone(info))
        }
    }

    abstract fun onCallExtrasUpdated(call: Call, extras: CallExtras)
}
